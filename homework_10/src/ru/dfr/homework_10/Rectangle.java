package ru.dfr.homework_10;

public class Rectangle extends Figure{

    protected int x2, x3, x4;
    protected int y2, y3, y4;

    public Rectangle(int x, int y, int x2, int x3, int x4, int y2, int y3, int y4) {
        super(x, y);
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.y2 = y2;
        this.y3 = y3;
        this.y4 = y4;
    }

    @Override
    public void printCoord() {
        System.out.printf("Новые координаты: x1, y1 = (%d, %d), x2, y2 = (%d, %d), x3, y3 = (%d, %d), x4, y4 = (%d, %d)\n", getX(), getY(), getX2(), getY2(), getX3(), getY3(), getX4(), getY4());
    }

    @Override
    public int minX() {
        int[] arrayCoordX = {this.getX(), this.getX2(), this.getX3(), this.getX4()};
        int minEl = arrayCoordX[0];
        int minId = 0;
        for (int i = 1 ; i < arrayCoordX.length; i++) {
            if (arrayCoordX[i] < minEl) {
                minEl = arrayCoordX[i];
                minId = i;
            }
        }
        return arrayCoordX[minId];
    }

    @Override
    public int maxX() {
        int[] arrayCoordX = {this.getX(), this.getX2(), this.getX3(), this.getX4()};
        int maxEl = arrayCoordX[0];
        int maxId = 0;
        for (int i = 1 ; i < arrayCoordX.length; i++) {
            if (arrayCoordX[i] > maxEl) {
                maxEl = arrayCoordX[i];
                maxId = i;
            }
        }
        return arrayCoordX[maxId];
    }

    @Override
    public int minY() {
        int[] arrayCoordY = {this.getY(), this.getY2(), this.getY3(), this.getY4()};
        int minEl = arrayCoordY[0];
        int minId = 0;
        for (int i = 1 ; i < arrayCoordY.length; i++) {
            if (arrayCoordY[i] < minEl) {
                minEl = arrayCoordY[i];
                minId = i;
            }
        }
        return arrayCoordY[minId];
    }

    @Override
    public int maxY() {
        int[] arrayCoordY = {this.getY(), this.getY2(), this.getY3(), this.getY4()};
        int maxEl = arrayCoordY[0];
        int maxId = 0;
        for (int i = 1 ; i < arrayCoordY.length; i++) {
            if (arrayCoordY[i] > maxEl) {
                maxEl = arrayCoordY[i];
                maxId = i;
            }
        }
        return arrayCoordY[maxId];
    }

    @Override
    public double getPerimeter() {
        return (double) 2 * (this.maxX() - this.minX()) + 2 * (this.maxY() - this.minY());
    }

    @Override
    public void printPerimeter() {
        System.out.printf("Периметр прямоугольника = %.2f\n", this.getPerimeter());
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    public void setX(int x) {
        super.setX(x);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
    }

    public int getX2() {
        return x2;
    }

    public int getX3() {
        return x3;
    }

    public int getX4() {
        return x4;
    }

    public int getY2() {
        return y2;
    }

    public int getY3() {
        return y3;
    }

    public int getY4() {
        return y4;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public void setX3(int x3) {
        this.x3 = x3;
    }

    public void setX4(int x4) {
        this.x4 = x4;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public void setY3(int y3) {
        this.y3 = y3;
    }

    public void setY4(int y4) {
        this.y4 = y4;
    }
}


