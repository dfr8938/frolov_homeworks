package ru.dfr.homework_10;

public class Square extends Rectangle implements Move{

    protected int a;

    public Square(int x, int y, int x2, int x3, int x4, int y2, int y3, int y4) {
        super(x, y, x2, x3, x4, y2, y3, y4);

        this.a = this.maxX() - this.minX();
    }

    @Override
    public void getMoving(int x, int y) {
        // x3, y3
        this.setX3(x);
        this.setY3(y);
        // x4, y4
        this.setX4(this.getX3());
        this.setY4(this.getY3() - this.a);
        // x2, y2
        this.setX2(this.getX3() - this.a);
        this.setY2(this.getY3());
        // x1, y1
        this.setX(this.getX3() - this.a);
        this.setY(this.getY3() - this.a);

        this.printCoord();
    }

    @Override
    public void printCoord() {
        System.out.printf("Новые координаты вершин квадрата: x1, y1 = (%d, %d), x2, y2 = (%d, %d), x3, y3 = (%d, %d), x4, y4 = (%d, %d)\n", getX(), getY(), getX2(), getY2(), getX3(), getY3(), getX4(), getY4());
    }

    @Override
    public double getPerimeter() {
        return (double) 4 * (this.maxX() - this.minX());
    }

    @Override
    public void printPerimeter() {
        System.out.printf("Периметр квардата = %.2f\n", this.getPerimeter());
    }
}

