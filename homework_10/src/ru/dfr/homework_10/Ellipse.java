package ru.dfr.homework_10;

public class Ellipse extends Figure{

    protected int x2, x3, x4;
    protected int y2, y3, y4;
    protected int r1;
    protected int r2;

    public Ellipse(int x, int y, int x2, int x3, int x4, int y2, int y3, int y4, int r1, int r2) {
        super(x, y);
        this.x2 = x2;
        this.x3 = x3;
        this.x4 = x4;
        this.y2 = y2;
        this.y3 = y3;
        this.y4 = y4;
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public void printCoord() {
        System.out.printf("Новые координаты: x1, y1 = (%d, %d), x2, y2 = (%d, %d), x3, y3 = (%d, %d), x4, y4 = (%d, %d)\n", getX(), getY(), getX2(), getY2(), getX3(), getY3(), getX4(), getY4());
    }

    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt(((Math.pow(this.r1, 2) + Math.pow(this.r2, 2)) / 2));
    }

    @Override
    public void printPerimeter() {
        System.out.printf("Периметр эллипса = %.2f\n", this.getPerimeter());
    }

    @Override
    public int getX() {
        return super.getX();
    }

    @Override
    public int getY() {
        return super.getY();
    }

    @Override
    public void setX(int x) {
        super.setX(x);
    }

    @Override
    public void setY(int y) {
        super.setY(y);
    }

    public int getX2() {
        return x2;
    }

    public void setX2(int x2) {
        this.x2 = x2;
    }

    public int getX3() {
        return x3;
    }

    public void setX3(int x3) {
        this.x3 = x3;
    }

    public int getX4() {
        return x4;
    }

    public void setX4(int x4) {
        this.x4 = x4;
    }

    public int getY2() {
        return y2;
    }

    public void setY2(int y2) {
        this.y2 = y2;
    }

    public int getY3() {
        return y3;
    }

    public void setY3(int y3) {
        this.y3 = y3;
    }

    public int getY4() {
        return y4;
    }

    public void setY4(int y4) {
        this.y4 = y4;
    }

    public int getR1() {
        return r1;
    }

    public void setR1(int r1) {
        this.r1 = r1;
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }
}

