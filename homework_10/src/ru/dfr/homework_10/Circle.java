package ru.dfr.homework_10;

public class Circle extends Ellipse implements Move{

    public Circle(int x, int y, int x2, int x3, int x4, int y2, int y3, int y4, int r1) {
        super(x, y, x2, x3, x4, y2, y3, y4, r1, r1);
    }

    @Override
    public void getMoving(int x, int y) {
        // x4, y4
        this.setX4(x);
        this.setY4(y);
        // x3, y3
        this.setX3(this.getX4() - this.r1);
        this.setY3(this.getY4() + this.r1);
        // x2, y2
        this.setX2(this.getX3() - this.r1);
        this.setY2(this.getY3() - this.r1);
        // x1, y1
        this.setX(this.getX2() + this.r1);
        this.setY(this.getY2() - this.r1);

        this.printCoord();
    }

    @Override
    public void printCoord() {
        System.out.printf("Новые координаты вершин круга:    x1, y1 = (%d, %d), x2, y2 = (%d, %d), x3, y3 = (%d, %d), x4, y4 = (%d, %d)\n", getX(), getY(), getX2(), getY2(), getX3(), getY3(), getX4(), getY4());
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * this.r1;
    }

    @Override
    public void printPerimeter() {
        System.out.printf("Периметр круга = %.2f\n", this.getPerimeter());
    }
}

