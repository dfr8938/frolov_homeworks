package ru.dfr.homework_10;

public class Main {
    public static void main(String[] args) {

        int x = 4;
        int y = 4;

        Move[] move = {
                new Square(1, 1, 1, 3, 3, 3, 3, 1),
                new Circle(1, 1, 1, 3, 3, 3, 3, 1, 1)
        };

        for (Move value : move) {
            value.getMoving(x, y);
        }
    }
}
