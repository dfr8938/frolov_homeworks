package ru.dfr.homework_10;

public abstract class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int minX() {
        return 0;
    }

    public int maxX() {
        return 0;
    }

    public int minY() {
        return 0;
    }

    public int maxY() {
        return 0;
    }

    public void printCoord() {}

    public abstract double getPerimeter();

    public void printPerimeter(){}

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}

