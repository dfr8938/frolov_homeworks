package ru.dfr.junit;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {
        @ParameterizedTest(name = "return {2} on {0} - {1}")
        @CsvSource(value = {"18, 12, 6", "36, 12, 12", "64, 48, 16", "9, 12, 3", "12, 18, 6", "48, 64, 16", "12, 36, 12"})
        public void return_correct_gcd(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return {2} on {0} - {1}")
        @CsvSource(value = {"17, 12, 1", "37, 12, 1", "64, 47, 1", "7, 12, 1", "11, 18, 1", "47, 64, 1", "11, 36, 1"})
        public void return_incorrect_gcd(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "return {2} on {0} - {1}")
        @CsvSource(value = {"0, 0", "-1, -1", "0, -1", "-1, 0", "-1, 12", "12, -1"})
        public void return_throws_gcd(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }
    }
}