package ru.dfr.homework_17;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*
        ** На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
         */
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        /*
        ** Слово - символы, ограниченные пробелами справа и слева
        ** Использовать Map, string.split(" ") - для деления текста по словам.
        */
        String[] arrayString = str.split(" ");

        Map<String, Integer> map = addInMap(arrayString);

        /*
        ** Вывести:
        ** Слово - количество раз
        */
        printMap(map);
    }

    public static void printMap(Map<String, Integer> map) {
        for (Map.Entry entry : map.entrySet()) {
            System.out.printf("'%s' - %d\n", entry.getKey(), entry.getValue());
        }
    }

    public static Map addInMap(String[] array) {
        Map<String, Integer> map = new HashMap<>();

        for (String a : array) {
            // если ключ отсутствует в map, то он записывается в словарь
            // в том, числе также записывается первые ключ - значение в словарь
            if (!map.containsKey(a)) {
                map.put(a, 1);
            } else {
                // если ключ присутствует в словаре, то к значение увеличивается на 1
                map.put(a, map.get(a) + 1);
            }
        }

        return map;
    }

}
