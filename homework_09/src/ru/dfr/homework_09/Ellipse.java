package ru.dfr.homework_09;

public class Ellipse extends Figure{

    protected double r1;
    protected double r2;

    public Ellipse(int x, int y, double r1, double r2) {

        super(x, y);

        this.r1 = r1;
        this.r2 = r2;
    }

    public double getPerimeter() {
        return 2 * Math.PI * Math.sqrt(((Math.pow(this.r1, 2) + Math.pow(this.r2, 2)) / 2));
    }

    public void printFigure() {
        System.out.printf("Периметр эллипса: %.2f\n", this.getPerimeter());
    }

}
