package ru.dfr.homework_09;

public class Circle extends Ellipse{

    public Circle(int x, int y, double r1) {

        super(x, y, r1, r1);
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.r1;
    }

    public void printFigure() {
        System.out.printf("Периметр круга: %.2f\n", this.getPerimeter());
    }
}
