package ru.dfr.homework_09;

public class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getPerimeter() {
        return 0;
    }

    public void printFigure() {
        System.out.printf("Периметр фигуры: %.2f\n", this.getPerimeter());
    }
}
