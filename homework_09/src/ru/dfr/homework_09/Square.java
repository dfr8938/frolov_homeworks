package ru.dfr.homework_09;

public class Square extends Rectangle{

    public Square(int x, int y, double a) {
        super(x, y, a, a);
    }

    public double getPerimeter() {
        return 4 * this.getA();
    }

    public void printFigure() {
        System.out.printf("Периметр квадрата: %.2f\n", this.getPerimeter());
    }
}
