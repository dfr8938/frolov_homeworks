package ru.dfr.homework_09;

public class Main {

    public static void main(String[] args) {
        Figure figure = new Figure(1, 2);
        figure.printFigure();

        Ellipse ellipse = new Ellipse(1, 2, 3, 4);
        ellipse.printFigure();

        Circle circle = new Circle(1, 2, 3);
        circle.printFigure();

        Rectangle rectangle = new Rectangle(1, 2, 3, 4);
        rectangle.printFigure();

        Square square = new Square(1, 2, 3);
        square.printFigure();
    }
}
