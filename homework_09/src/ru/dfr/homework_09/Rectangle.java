package ru.dfr.homework_09;

public class Rectangle extends Figure{

    protected double a;
    protected double b;

    public Rectangle(int x, int y, double a, double b) {

        super(x, y);

        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        return 2 * (this.getA() + this.getB());
    }

    public void printFigure() {
        System.out.printf("Периметр прямоугольника: %.2f\n", this.getPerimeter());
    }

    public double getA() {
        return this.a;
    }

    public double getB() {
        return this.b;
    }
}
