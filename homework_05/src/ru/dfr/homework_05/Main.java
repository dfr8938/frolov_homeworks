package ru.dfr.homework_05;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        ArrayList<Integer> list = new ArrayList<>();

        for (int num = sc.nextInt(); num != -1; num = sc.nextInt()) {

            String str = Integer.toString(num);
            int lStr = str.length();

            int[] res = new int[lStr];

            for (int i = 0; i < res.length; i++) {
                res[i] = str.charAt(i) - '0';
            }

            arraySort(res);

            list.add(res[0]);
        }

        int[] listArr = new int[list.size()];
        for(int i = 0; i < list.size(); i++) {
            listArr[i] = list.get(i);
        }

        arraySort(listArr);

        System.out.printf("Ответ: %d", listArr[0]);
    }

    private static void arraySort(int[] listArr) {
        boolean sort = false;
        int target;

        while (!sort) {
            sort = true;
            for (int i = 0; i < listArr.length - 1; i++) {
                if (listArr[i] > listArr[i + 1]) {
                    sort = false;
                    target = listArr[i];
                    listArr[i] = listArr[i + 1];
                    listArr[i + 1] = target;
                }
            }
        }
    }
}
