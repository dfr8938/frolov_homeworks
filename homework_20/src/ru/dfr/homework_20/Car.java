package ru.dfr.homework_20;

public class Car {

    // o001aa111|Camry|Black|133|82000
    private String number;
    private String brand;
    private String color;
    private int mileage;
    private int price;

    public Car(String number, String brand, String color, int mileage, int price) {
        this.number = number;
        this.brand = brand;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }

    public String getNumber() {
        return number;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }
}
