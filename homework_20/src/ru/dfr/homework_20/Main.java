package ru.dfr.homework_20;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        String file = "cars.txt";
        CarsList carsList = new CarsList(file);

        List<Car> cars = carsList.createCarsList();

        // Задание 1:
        // вывод всех номеров автомобилей
        // черного цвета
        // или
        // с нулевым пробегом
        System.out.println("1. Номера автомобилей черного цвета или с нулевым пробегом:");
        findCarsByColorAndMileage(cars, "Black");

        // Задание 2:
        // вывод количества уникальных моделей
        // в ценовом диапазоне от от 700 до 800 тыс
        System.out.printf("2. Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.: %d\n",
                uniqueModelsCars(cars, 700000, 800000));

        // Задание 3:
        // вывод цвета автомобиля с минимальной
        // стоимостью
        System.out.printf("3. Вывести цвет автомобиля с минимальной стоимостью: %s\n", sortByPrice(cars));

        // Задаие 4:
        // вывод средней стоимости Camry:
        System.out.printf("4. Средняя стоимость Camry: %.2f\n",
                averageCostOfCamry(cars, "Camry"), "Camry");
    }

    // находим все машины черного цвета или с нулевым пробегом
    public static void findCarsByColorAndMileage(List<Car> cars, String color) {
        cars.stream()
                .filter(car -> car.getColor().equals(color) || car.getMileage() == 0)
                .map(Car::getNumber)
                .forEach(System.out::println);
    }
    // находим автомобили в ценовом диапазоне от 700 до 800 тыс
    public static int uniqueModelsCars(List<Car> cars, int startPrice, int endPrice) {
        List<Car> listCars = cars.stream()
                .distinct()
                .filter(car -> car.getPrice() <= endPrice && car.getPrice() >= startPrice)
                .collect(Collectors.toList());
        return listCars.size();
    }
    // находим минимальную стоимость автомобиля, затем выводим его цвет автомобиля
    public static String sortByPrice(List<Car> cars) {
        Car car = cars.stream().min(Comparator.comparing(Car::getPrice)).orElse(null);
        return car.getColor();
    }
    // находим среднюю стоимость Camry
    // отфильтровываем все машины по brand = "Camry" filter(car -> car.getBrand().equals(brand)
    // получаем стрим цен всех Camry mapToInt(Car::getPrice)
    // находим среднюю стоимость average().getAsDouble()
    public static double averageCostOfCamry(List<Car> cars, String brand) {
         return cars.stream()
                .filter(car -> car.getBrand().equals(brand))
                .mapToInt(Car::getPrice).average().getAsDouble();
    }

    public static void printList(List<Car> cars) {
        for(Car car : cars) {
            System.out.printf("%s %s %s %d %d\n", car.getNumber(),
                    car.getBrand(), car.getColor(),
                    car.getMileage(), car.getPrice());
        }
    }
}
