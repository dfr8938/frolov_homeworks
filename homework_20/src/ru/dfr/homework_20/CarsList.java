package ru.dfr.homework_20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CarsList {

    private String file;

    public CarsList(String file) {
        this.file = file;
    }

    public List<Car> createCarsList() {

        List<Car> cars = new ArrayList<>();

        Reader reader = null;
        BufferedReader bufferReader = null;
        try {
            reader = new FileReader(file);
            bufferReader = new BufferedReader(reader);
            String lines = bufferReader.readLine();

            while (lines != null) {
                String[] linesPath = lines.split("\\|");
                Car car = new Car(linesPath[0], linesPath[1], linesPath[2],
                        Integer.parseInt(linesPath[3]), Integer.parseInt(linesPath[4]));
                cars.add(car);
                lines = bufferReader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {}
            }
            if (bufferReader != null) {
                try {
                    bufferReader.close();
                } catch (IOException ignored) {}
            }
        }
        return cars;
    }
}
