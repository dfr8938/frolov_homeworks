package ru.dfr.jdbc;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private int id;
    private String title;
    private String description;
    private double price;
    private int quantity;
}
