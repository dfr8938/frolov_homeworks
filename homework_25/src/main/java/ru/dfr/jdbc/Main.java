package ru.dfr.jdbc;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {

    private static final String URL = "jdbc:postgresql://localhost:5432/homework_24";
    private static final String USER = "postgres";
    private static final String PASSWORD = "1234";

    public static void main(String[] args) {
        ProductsRepositoryImpliment productsRepository =
                new ProductsRepositoryImpliment(dataSource(URL, USER, PASSWORD));

        System.out.println("** findAll() **");
        outputMethodFindAll(productsRepository);

        System.out.println("** findAllByPrice() **");
        outputMethodFindAllByPrice(productsRepository, 152999);

        System.out.println("** findAllByOrdersCount() **");
        outputMethodFindAllByOrdersCount(productsRepository, 2);
    }

    public static void outputMethodFindAll(ProductsRepositoryImpliment productsRepository) {
        System.out.println(productsRepository.findAll());
    }

    public static void outputMethodFindAllByPrice(ProductsRepositoryImpliment productsRepository, double d) {
        System.out.println(productsRepository.findAllByPrice(d));
    }

    public static void outputMethodFindAllByOrdersCount(ProductsRepositoryImpliment productsRepository, int n) {
        System.out.println(productsRepository.findAllByOrdersCount(2));
    }

    public static DataSource dataSource(String url, String user, String pass) {
        return new DriverManagerDataSource(url, user, pass);
    }
}