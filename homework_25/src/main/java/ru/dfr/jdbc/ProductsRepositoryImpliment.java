package ru.dfr.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;
import java.util.stream.Collectors;

public class ProductsRepositoryImpliment implements ProductsRepository {
    // language=SQL
    private static final String SQL_SELECT_ALL = "SELECT id, title, description, price, quantity FROM product ORDER BY id";
    // language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE = "SELECT * FROM product WHERE price = ? ORDER BY id";
    // language=SQL
    private static final String SQL_SELECT_ALL_BY_ORDERS_COUNT = "SELECT (SELECT id FROM product WHERE id = orders.product_id), (SELECT title FROM product WHERE id = orders.product_id), (SELECT description FROM product WHERE id = orders.product_id), (SELECT price FROM product WHERE id = orders.product_id), (SELECT quantity FROM product WHERE id = orders.product_id) FROM orders WHERE number_of_goods = ?";

    private final JdbcTemplate jdbcTemplate;

    public ProductsRepositoryImpliment(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public RowMapper<Product> productRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String title = row.getString("title");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int quantity = row.getInt("quantity");
        return new Product(id, title, description, price, quantity);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE, productRowMapper, price);
    }

    @Override
    public List<Product> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_ORDERS_COUNT, productRowMapper, ordersCount);
    }
}
