package ru.dfr.homework_07;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static int counts = Integer.MAX_VALUE;
    public static int sizeArray = 201;
    public static int min = -100;
    public static int max = 100;

    public static void main(String[] args) {

        int[] array = inputArray();
        if (array.length == 0) {
            System.out.print("Массив пуст! ");
            System.out.println("[]");
        } else {
            printArray(array);
            printOutput(array);
        }

    }

    public static void printOutput(int[] array) {
        System.out.printf("Число, которое присутствует в последовательности минимальное количество раз: %d\n",
                outputMinNumber(countsArray(array)));
    }

    public static int outputMinNumber(int[] array) {

        int minValue = counts;
        int minElement = 0;

        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                if (array[i] != 0) {
                    minValue = array[i];
                    minElement = i - max;
                }
            }
        }
        return minElement;
    }

    public static int[] countsArray(int[] array) {

        int[] newArray = new int[sizeArray];
        for(int i = 0; i < array.length; i++) {
            newArray[array[i] + max]++;
        }
        return newArray;
    }

    public static int[] inputArray() {

        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> list = new ArrayList<>();

        for (int n = sc.nextInt(); n != -1; n = sc.nextInt()) {
            if(n <= max && n >= min) {
                list.add(n);
            }
        }

        int[] array = new int[list.size()];

        for (int i = 0; i < array.length; i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public static void printArray(int[] array) {
        System.out.print("[");
        for (int i = 0; i < array.length; i++) {
            if (i != array.length - 1) System.out.printf("%d, ", array[i]);
            else System.out.printf("%d]\n", array[i]);
        }
    }
}
