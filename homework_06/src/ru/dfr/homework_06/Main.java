package ru.dfr.homework_06;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] arr = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};

        System.out.println(indexNumberInArray(arr, 14));
        System.out.println(Arrays.toString(sortZero(arr)));
    }

    public static int indexNumberInArray(int[] arr, int num) {

        int n = -1;
        for(int i = 0; i < arr.length; i++) {
            if (arr[i] == num) {
                n = i;
            }
        }
        return n;
    }

    public static int[] sortZero(int[] arr) {

        boolean sort = false;
        int target;

        while (!sort) {
            sort = true;
            for (int i = arr.length - 1; i > 0; i--) {
                if (arr[i] != 0 && arr[i - 1] == 0) {
                    sort = false;
                    target = arr[i];
                    arr[i] = arr[i - 1];
                    arr[i - 1] = target;
                }
            }
        }
        return arr;
    }
}
