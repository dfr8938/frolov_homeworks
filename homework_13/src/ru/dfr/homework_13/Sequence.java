package ru.dfr.homework_13;

import java.util.ArrayList;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {

        ArrayList<Integer> tmp = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                tmp.add(array[i]);
            }
        }
        int[] res = new int[tmp.size()];
        for (int i = 0 ; i < tmp.size(); i++) {
            res[i] = tmp.get(i);
        }
        return res;
    }
}
