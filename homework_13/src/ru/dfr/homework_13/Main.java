package ru.dfr.homework_13;

import java.util.Arrays;

public class Main {

    /*
    ** В main в качестве condition подставить:
    ** - проверку на четность элемента
    ** - проверку, является ли сумма цифр элемента четным числом.
    */
    public static void main(String[] args) {

        int[] array = {10, 15, 153, 5, 21, 55, 16, 99, 100, 1, 22, -22, 134, -4, -55};

        int[] result = Sequence.filter(array, number -> {
            if (isEven(number) && isEven(sumNumbers(number))) return true;
            return false;
        });

        if (result.length == 0) {
            System.out.println("Массив пуст. Нет элементов удовлетворяющих заявленным условиям");
        } else {
            System.out.print("Массив, удовлетворяющий заявленным требованиям: ");
            System.out.println(Arrays.toString(result));
        }
    }

    public static boolean isEven(int number) {
        if (number % 2 == 0) return true;
        return false;
    }

    public static int sumNumbers(int number) {

        if (number < 0) {
            number = Math.abs(number);
        }

        String str = Integer.toString(number);
        int result = 0;

        for (int i = 0; i < str.length(); i++) {
            result += str.charAt(i) - '0';
        }

        return result;
    }
}
