package ru.dfr.homework_28.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.dfr.homework_28.forms.ProductForm;
import ru.dfr.homework_28.models.Product;
import ru.dfr.homework_28.repositories.ProductsRepository;

import java.util.List;

@Component
public class ProductsService implements ProductService {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsService(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public void addProduct(ProductForm form) {
        Product product = Product.builder()
                .title(form.getTitle())
                .descriptionFull(form.getDescriptionFull())
                .descriptionPromo(form.getDescriptionPromo())
                .price(form.getPrice())
                .urlImage(form.getUrlImage())
                .quantity(form.getQuantity())
                .build();

        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAllProducts();
    }
}
