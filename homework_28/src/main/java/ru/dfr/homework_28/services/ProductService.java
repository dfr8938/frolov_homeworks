package ru.dfr.homework_28.services;

import ru.dfr.homework_28.forms.ProductForm;
import ru.dfr.homework_28.models.Product;

import java.util.List;

public interface ProductService {
    void addProduct(ProductForm form);
    List<Product> getAllProducts();
}
