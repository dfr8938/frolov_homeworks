package ru.dfr.homework_28.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    private Integer id;
    private String title;
    private String descriptionFull;
    private String descriptionPromo;
    private Double price;
    private String urlImage;
    private Integer quantity;
}
