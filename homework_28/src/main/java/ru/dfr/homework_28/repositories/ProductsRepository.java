package ru.dfr.homework_28.repositories;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.dfr.homework_28.models.Product;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductsRepository implements ProductRepository{
    // language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "SELECT id, title, description_full, description_promo, price, url_image, quantity FROM goods ORDER BY id";
    // language=SQL
    private static final String SQL_INSERT_PRODUCT = "INSERT INTO goods(title, description_full, description_promo, price, url_image, quantity) VALUES(?, ?, ?, ?, ?, ?)";
    private final JdbcTemplate jdbcTemplate;

    public ProductsRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public final RowMapper<Product> productMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String title = row.getString("title");
        String descriptionFull = row.getString("description_full");
        String descriptionPromo = row.getString("description_promo");
        Double price = row.getDouble("price");
        String urlImage = row.getString("url_image");
        int quantity = row.getInt("quantity");

        return new Product(id, title, descriptionFull, descriptionPromo, price, urlImage, quantity);
    };

    @Override
    public List<Product> findAllProducts() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT_PRODUCT,
                product.getTitle(), product.getDescriptionFull(),
                product.getDescriptionPromo(),
                product.getPrice(), product.getUrlImage(),
                product.getQuantity());
    }
}
