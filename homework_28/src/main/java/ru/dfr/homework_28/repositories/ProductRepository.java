package ru.dfr.homework_28.repositories;

import ru.dfr.homework_28.models.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> findAllProducts();
    void save(Product product);
}
