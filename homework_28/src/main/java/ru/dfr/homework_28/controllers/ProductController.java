package ru.dfr.homework_28.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.dfr.homework_28.forms.ProductForm;
import ru.dfr.homework_28.models.Product;
import ru.dfr.homework_28.services.ProductsService;

import java.util.List;

@Controller
public class ProductController {

    private final ProductsService productsService;

    @Autowired
    public ProductController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/goods")
    public String getProductsPage(Model model) {
        List<Product> goods = productsService.getAllProducts();
        model.addAttribute("goods", goods);
        return "goods";
    }

    @PostMapping("/goods")
    public String addProduct(ProductForm form) {
        productsService.addProduct(form);
        return "redirect:/goods";
    }
}
