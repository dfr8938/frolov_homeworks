package ru.dfr.homework_28.forms;

import lombok.Data;

@Data
public class ProductForm {
    private String title;
    private String descriptionFull;
    private String descriptionPromo;
    private Double price;
    private String urlImage;
    private Integer quantity;
}
