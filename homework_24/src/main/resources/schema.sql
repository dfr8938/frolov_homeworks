-- создание таблицы "Товар"
CREATE TABLE product (
                         id SERIAL PRIMARY KEY,
                         title VARCHAR(255) NOT NULL,
                         description TEXT NOT NULL,
                         price DECIMAL check (price >= 0) NOT NULL,
                         quantity INTEGER check (quantity >= 0) NOT NULL
);
-- создание таблицы "Заказчик"
CREATE TABLE customer (
                          id SERIAL PRIMARY KEY,
                          name VARCHAR(20) NOT NULL,
                          surname VARCHAR(20) NOT NULL,
                          patronymic VARCHAR(50),
                          date_of_birth DATE NOT NULL
);
-- Создание таблицы "Заказ"
CREATE TABLE orders (
                        id SERIAL PRIMARY KEY,
                        product_id INTEGER NOT NULL,
                        customer_id INTEGER NOT NULL,
                        date_order DATE NOT NULL,
                        number_of_goods INTEGER NOT NULL,
                        FOREIGN KEY (product_id) REFERENCES product (id),
                        FOREIGN KEY (customer_id) REFERENCES customer (id)
);
-- заколнение таблицы "Товар"
INSERT INTO product(title, description, price, quantity)
VALUES ('13.3" Ноутбук Apple MacBook Air серый',
        'Нужен портативный помощник в работе и развлечениях? Ноутбук Apple MacBook Air вы сможете с собой взять даже в самолет! Он весит всего 1.29 кг, а его параметры составляют 21.2x30 см. Диагональ экрана приравнивается к 13.3" или 34 см. Он изготовлен на основе технологии IPS и способен воспроизводить картинку с разрешением до 2560x1600 пикселей. Этот параметр соответствует формату WQXGA.
Производитель оснастил ноутбук самым эффективным 8-ядерным процессором M1. Это значит, что с ним можно профессионально редактировать фото и видео, проходить ресурсоемкие игры. M1 шутя выполняет все повседневные задачи пользователя, распределяя их между ядрами. При этом он расходует до 10 раз меньше энергии, чем другие модели производителя. За счет этого работает устройство до 18 ч без подзарядки! Переживать за личные данные не придется – ноутбук Apple MacBook Air оснащен сканером отпечатка пальцев. SSD-накопитель модели обладает объемом 256 ГБ.',
        104999, 101);
INSERT INTO product(title, description, price, quantity)
VALUES ('13.3" Ноутбук Apple MacBook Air серебристый',
        'Ноутбук Apple MacBook Air в тонком (1.6 см) и изящном корпусе из металла получил от производителя все необходимое, чтобы стать надежным ассистентом в работе и развлечениях. Его основа – это 8-ядерный процессор M1, дополненный видеокартой M1 7-core и интегрированной оперативной памятью на 16 ГБ. Для удобства работы в плохо освещенных помещениях клавиатура дополнена подсветкой.
Любите безукоризненное изображение? Действительно, при просмотре фильмов и обработке фотографий это очень важно. С ноутбуком Apple MacBook Air вы точно получите безупречную «картинку». Он оснащен экраном IPS диагональю 13.3 дюйма с разрешением 2560x1600 пикселей и глянцевым покрытием. Высокоскоростной выход в интернет обеспечен модулем Wi-Fi 6 (802.11ax). За сохранность личных данных можете не переживать – сканер отпечатка пальцев не позволит посторонним совать нос в ваши дела.',
        104999, 32);
INSERT INTO product(title, description, price, quantity)
VALUES ('13.3" Ноутбук Apple MacBook Air серый',
        '13.3" Ноутбук Apple MacBook Air – неповторимое сочетание стиля, высокой производительности и автономности. Вы можете монтировать сложные видеопроекты, профессионально обрабатывать фото, создавать приложения или просто работать с документами и общаться в социальных сетях – нет задач, которые оказались бы не по силам этому компьютеру. И все это – на протяжении более 15 часов от аккумулятора.
В основе Apple MacBook Air используется фирменный процессор M1, который выполнен на ARM-архитектуре и сочетает в себе 8 вычислительных и 7 графических ядер. Благодаря этому обеспечивается удивительная производительность при малом энергопотреблении. Также в процессор встроено 16 ГБ временной памяти, что в совокупности обеспечивает минимальные задержки при записи и чтении данных и позволяет выполнять операции на высокой скорости. Для хранения данных предусмотрено 512 ГБ постоянной и скоростной памяти. Благодаря такому сочетанию компьютер позволит вам легко и комфортно реализовывать любые творческие и рабочие проекты.
Другим достоинством Apple MacBook Air является его экран диагональю 13.3”. Он выполнен по технологии IPS, имеет полный охват цветового пространства DCI-P3 и отличается разрешением 2560x1600 пикселей, благодаря чему картинка порадует вас естественными и насыщенными цветами, а также высокой детализацией.
Коммуникационные возможности MacBook Air представлены модулями Wi-Fi 6 и Bluetooth 5.0 Для подключения периферии предусмотрено 2 порта Thunderbolt 3.',
        123599, 2);
INSERT INTO product(title, description, price, quantity)
VALUES ('13.3" Ноутбук Apple MacBook Pro серый',
        'Ноутбук Apple MacBook Pro выполнен в металлическом корпусе серого цвета толщиной 15.6 мм и весом 1.4 кг, поэтому станет стильным и надежным спутником на каждый день. На экране IPS диагональю 13.3 дюймов с разрешающей способностью 2560x1600 пикселей отображается четкое изображение с насыщенными и яркими цветами. Датчик освещенности в автоматическом режиме настраивает яркость дисплея.
Apple MacBook Pro получил мощную и энергоэффективную начинку. Аппаратная платформа во главе с процессором M1 и в дополнении 16 ГБ памяти ОЗУ обеспечивает высокие показатели производительности при решении самых разных компьютерных задач. В то же время он обеспечивает около 20 часов автономной работы на основе литий-полимерного аккумулятора. Среди прочих особенностей отмечаются клавиатура с белой светодиодной подсветкой, веб-камера, востребованные проводные и беспроводные интерфейсы, сканер отпечатков пальцев.',
        152999, 78);
INSERT INTO product(title, description, price, quantity)
VALUES ('21.5" Моноблок Apple iMac Retina 4K',
        'Моноблок Apple iMac Retina 4K воплощает в себе фирменный элегантный стиль и высокий уровень продуктивности для комфортного решения различных компьютерных задач. Расположенные в тонком корпусе процессор Intel Core i5 с 6 ядрами, дискретный графический адаптер AMD Radeon Pro 560x, 16 ГБ памяти ОЗУ и накопитель SSD на 256 ГБ подчеркивают производительность и быстродействие системы. Моноблок получил дисплей Retina диагональю 21.5 дюйма, основанный на матрице IPS с разрешающей способностью 4096x2304 пикселей.
Интегрированная акустическая система воспроизводит четкий звук. Для общения по видеосвязи имеются веб-камера и микрофон. Набор проводных интерфейсов представлен видеовыходом HDMI, 2 разъемами Thunderbolt 3, разъемом DisplayPort, а также разъемами аудио. Помимо этого, реализованы технологии беспроводной связи. В комплекте с моноблоком Apple iMac Retina 4K поставляются мышь и клавиатура с беспроводным подключением.',
        148499, 56);
INSERT INTO product(title, description, price, quantity)
VALUES ('14" Ноутбук Acer Swift 1 SF114-34-P186 серебристый',
        'Работайте продуктивно в течение всего дня с мощным тонким ноутбуком, который удобно брать с собой куда угодно. 14-дюймовый IPS-экран порадует пользователя красочностью изображения, которое будет демонстрироваться на нем в формате FulHD (разрешение 1920x1080 пикселей).',
        28049, 15);
INSERT INTO product(title, description, price, quantity)
VALUES ('14" Ноутбук ASUS Laptop 14 L410MA-EK1394T белый',
        'Этот ноутбук создан для тех, кто хочет получить надежное и производительное компьютерное устройство с наиболее востребованным функционалом. Данная модель полностью удовлетворяет данные требования. Надежный накопитель предоставляет вам возможности для долговременного хранения необходимой виртуальной информации. Устройство оборудовано веб-камерой и микрофоном, благодаря которым вы сможете организовывать видеоконференции с партнерами по бизнесу и коллегами по работе.',
        29999, 3);
-- заполнение таблицы "Заказчик"
INSERT INTO customer(name, surname, patronymic, date_of_birth)
VALUES ('Виктор', 'Порфенов', 'Павлович', '1977-05-01');
INSERT INTO customer(name, surname, patronymic, date_of_birth)
VALUES ('Иван', 'Иванов', 'Иванович', '1989-05-30');
INSERT INTO customer(name, surname, patronymic, date_of_birth)
VALUES ('Петр', 'Панфилов', 'Максимович', '1983-02-22');
INSERT INTO customer(name, surname, patronymic, date_of_birth)
VALUES ('Екатерина', 'Брошкина', 'Ивановна', '1991-01-18');
INSERT INTO customer(name, surname, patronymic, date_of_birth)
VALUES ('Жанна', 'Сидорова', 'Васильевна', '2000-09-11');
INSERT INTO customer(name, surname, patronymic, date_of_birth)
VALUES ('Максим', 'Кукушкин', 'Данилович', '1963-08-21');
-- заполнение таблицы "Заказ"
INSERT INTO orders(product_id, customer_id, date_order, number_of_goods)
VALUES (1, 2, '2021-05-15', 3);
INSERT INTO orders(product_id, customer_id, date_order, number_of_goods)
VALUES (2, 2, '2021-05-15', 2);
INSERT INTO orders(product_id, customer_id, date_order, number_of_goods)
VALUES (3, 2, '2021-05-15', 2);
INSERT INTO orders(product_id, customer_id, date_order, number_of_goods)
VALUES (1, 1, '2021-05-15', 15);
INSERT INTO orders(product_id, customer_id, date_order, number_of_goods)
VALUES (2, 1, '2021-05-15', 15);
INSERT INTO orders(product_id, customer_id, date_order, number_of_goods)
VALUES (5, 4, '2021-05-15', 1);
-- обновление данных в таблице "Заказ"
UPDATE product SET quantity = quantity - (SELECT number_of_goods FROM orders WHERE id = 1) WHERE id = 1;
UPDATE product SET quantity = quantity - (SELECT number_of_goods FROM orders WHERE id = 2) WHERE id = 2;
UPDATE product SET quantity = quantity - (SELECT number_of_goods FROM orders WHERE id = 3) WHERE id = 3;
UPDATE product SET quantity = quantity - (SELECT number_of_goods FROM orders WHERE id = 1) WHERE id = 4;
UPDATE product SET quantity = quantity - (SELECT number_of_goods FROM orders WHERE id = 2) WHERE id = 5;
UPDATE product SET quantity = quantity - (SELECT number_of_goods FROM orders WHERE id = 5) WHERE id = 6;
-- вывод данных таблицы "Товар"
SELECT * FROM product ORDER BY price;
-- вывод данных таблицы "Заказчик"
SELECT surname AS "ФАМИЛИЯ", name AS "ИМЯ",
       patronymic AS "ОТЧЕСТВО",
       date_of_birth AS "ДАТА РОЖДЕНИЯ" FROM customer ORDER BY surname;
-- вывод данных таблицы "Заказ"
SELECT * FROM orders ORDER BY number_of_goods;
-- общая количество проданного товара
SELECT SUM(number_of_goods) AS "КОЛИЧЕСТВО ПРОДАННОГО ТОВАРА"
FROM orders;
-- вывод имен всех заказчиков
SELECT DISTINCT (SELECT surname FROM customer WHERE id = orders.customer_id) AS "ФАМИЛИЯ",
                (SELECT name FROM customer WHERE id = orders.customer_id) AS "ИМЯ",
                (SELECT patronymic FROM customer WHERE id = orders.customer_id) AS "ОТЧЕСТВО"
FROM orders;
-- вывод заказов заказчика с id = 2;
SELECT SUM(number_of_goods) FROM orders WHERE customer_id = 2;
-- вывод имен всех заказчиков, кто произвел более 2 покупок
SELECT DISTINCT (SELECT surname FROM customer WHERE id = orders.customer_id) AS "ФАМИЛИЯ",
                (SELECT name FROM customer WHERE id = orders.customer_id) AS "ИМЯ",
                (SELECT patronymic FROM customer WHERE id = orders.customer_id) AS "ОТЧЕСТВО",
                number_of_goods AS "КОЛИЧЕСТВО"
FROM orders WHERE number_of_goods > 2;
-- сумма выручки по каждой единице проданного товара
SELECT (SELECT title FROM product WHERE id = orders.product_id) AS "ТОВАР",
       number_of_goods AS "КОЛИЧЕСТВО",
       (number_of_goods * (SELECT price FROM product WHERE id = orders.product_id)) AS "ВЫРУЧКА",
       (SELECT price FROM product WHERE id = orders.product_id) AS "СТОИМОСТЬ ОДНОй ЕДИНИЦЫ" FROM orders;
-- заказы по моделям
SELECT (SELECT title FROM product WHERE id = orders.product_id),
       SUM(number_of_goods) FROM orders GROUP BY product_id;

-- вывод заказа по количеству
SELECT (SELECT title FROM product WHERE id = orders.product_id), number_of_goods FROM orders WHERE number_of_goods = 3;