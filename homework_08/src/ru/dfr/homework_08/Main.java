package ru.dfr.homework_08;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int lengthArrayHumans = 10;
        Human[] humans = new Human[lengthArrayHumans];

        for(int i = 0; i < lengthArrayHumans; i++) {

            System.out.printf("Введите имя human#%d: ", i + 1);
            String name = sc.nextLine();

            System.out.printf("Введите вес human#%d: ", i + 1);
            double weight = sc.nextDouble();

            sc.nextLine();

            Human human = new Human(name, weight);
            humans[i] = human;
        }

        sortWeight(humans);
        printArray(humans);
    }

    public static Human[] sortWeight(Human[] array) {

        boolean sort = false;
        Human target;

        while (!sort) {
            sort = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].weight > array[i + 1].weight) {
                    sort = false;
                    target = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = target;
                }
            }
        }
        return array;
    }

    public static void printArray(Human[] array) {
        for(int i = 0; i < array.length; i++) {
            System.out.println(array[i].toString());
        }
    }

}
