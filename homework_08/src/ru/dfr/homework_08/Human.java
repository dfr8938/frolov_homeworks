package ru.dfr.homework_08;

public class Human {

    String name;
    double weight;

    Human (String name, double weight) {
        this.name = name;
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
