package ru.dfr.homework_16.linked_list;

public class LinkedList<T> {

    private static class Node<T> {

        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }

    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) first = newNode;
        else last.next = newNode;
        last = newNode;
        size++;
    }

    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);
        if (size == 0) last = newNode;
        else newNode.next = first;
        first = newNode;
        size++;
    }

    /*
    ** Реализовать метод T get(int index) для LinkedList
     */
    public T get(int index) {

        if (index >= size || Math.abs(index) >= size) {
            index = size - 1;
        } else if (index < 0) {
            int tmp = size + index;
            index = tmp;
        }

        Node<T> element = first;
        for (int i = 0; i < index; i++) {
            element = element.next;
        }
        return element.value;
    }

    public int size() { return size; }

    public void toStringLinkedList() {
        System.out.print("[");
        Node<T> element = first;
        for (int i = 0; i <= size - 1; i++) {
            if (i == 0) {
                System.out.print(first.value);
                System.out.print(", ");
            } else if (i != size - 1) {
                element = element.next;
                System.out.print(element.value);
                System.out.print(", ");
            } else {
                element = element.next;
                System.out.print(element.value);
            }
        }
        System.out.printf("] [размер списка: %d]\n", size());
    }
}
