package ru.dfr.homework_16.linked_list;

public class Main {

    public static void main(String[] args) {

        LinkedList<Integer> linkedList = new LinkedList<>();

        linkedList.add(15);
        linkedList.add(23);
        linkedList.add(32);
        linkedList.add(14);
        linkedList.add(10);

        linkedList.addToBegin(77);

        linkedList.toStringLinkedList();
        System.out.printf("Искомый элемент списка: %d\n", linkedList.get(-1));
    }
}
