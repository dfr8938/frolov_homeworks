package ru.dfr.homework_16.array_list;

public class ArrayList<T> {

    private static final int DEFAULT_SIZE = 10;

    private T[] elements;
    private int size;

    public ArrayList () {
        this.elements = (T[]) new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    public void add (T element) {

        if (size == elements.length) {
            T[] oldElements = this.elements;
            this.elements = (T[]) new Object[oldElements.length + oldElements.length / 2];
            for (int i = 0; i < size; i++) {
                this.elements[i] = oldElements[i];
            }
        }
        this.elements[size] = element;
        size++;
    }

    public T get (int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else {
            return null;
        }
    }

    public void clear () {
        this.size = 0;
    }

    /*
    ** Реализовать removeAt(int index) для ArrayList
     */
    public void removeAt (int index) {

        if (index > size || Math.abs(index) > size) {
            index = size - 1;
        } else if (index < 0) {
            int tmp = size + index;
            index = tmp;
        }

        T[] oldElements = this.elements;
        this.elements = (T[]) new Object[size - 1];
        for (int i = 0; i < index; i++) {
            this.elements[i] = oldElements[i];
        }
        for (int i = index + 1, j = index; i < size; i++, j++) {
            this.elements[j] = oldElements[i];
        }
        size -= 1;

    }

    public void toStringArrayList() {
        System.out.print("[");
        for (int i = 0; i < size; i++) {
            if (i != size - 1) System.out.print(this.elements[i] + ", ");
            else System.out.print(this.elements[i]);
        }
        System.out.printf("] [размер списка: %d]\n", size);
    }
}
