package ru.dfr.homework_16.array_list;

public class Main {

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();

        list.add(12);
        list.add(15);
        list.add(122);
        list.add(1);
        list.add(2);
        list.add(172);
        list.add(712);
        list.add(5);
        list.add(78);
        list.add(91);
        list.add(100);
        list.add(999);
        list.add(45);
        list.add(8);
        list.add(20);
        list.add(45);
        list.add(51);
        list.toStringArrayList();

        list.removeAt(-1);
        list.toStringArrayList();

        list.removeAt(2);
        list.toStringArrayList();

        list.removeAt(-3);
        list.toStringArrayList();

        list.removeAt(-17);
        list.toStringArrayList();

        list.removeAt(-1);
        list.toStringArrayList();

        list.removeAt(-12);
        list.toStringArrayList();
    }
}
