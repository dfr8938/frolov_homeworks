package ru.dfr.homework_21;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {

        // Реализовать многопоточное суммирование элементов
        Random rand = new Random();
        Scanner sc = new Scanner(System.in);
        int numbersCount = sc.nextInt(); // 100
        int threadsCount = sc.nextInt(); // 2

        array = new int[numbersCount];
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(100);
        }

        int arraySum = 0;
        // сумма всех элементов массива в потоке main
        for (int j : array) {
            arraySum += j;
        }

        // шаг, с которым проходим по массиву
        int step = numbersCount / threadsCount;
        // итератор, для нахождения шага
        int count = numbersCount / threadsCount;
        // создаем массив потоков размером threadsCount
        SumThread[] sumThreads = new SumThread[threadsCount];
        for (int i = 0; i < threadsCount; i++) {
            if (i == 0) {
                SumThread thread = new SumThread(array, 0, step - 1);
                // записываем поток в массив SumThread[] sumThreads
                sumThreads[i] = thread;
            } else if (i != threadsCount - 1) {
                SumThread thread = new SumThread(array, step, (count + step) - 1);
                // записываем поток в массив SumThread[] sumThreads
                sumThreads[i] = thread;
                // каждый раз увеличиваем шаг
                step += count;
            } else {
                // последний поток забирает все оставшиеся элементы массива
                SumThread thread = new SumThread(array, step, array.length - 1);
                // записываем поток в массив SumThread[] sumThreads
                sumThreads[i] = thread;
            }
        }

        sums = new int[sumThreads.length];
        for (int i = 0; i < sumThreads.length; i++) {
            // запускаем потоки
            sumThreads[i].start();
            try {
                // блокируем вызывающий поток main
                sumThreads[i].join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
            int sum = sumThreads[i].sumNumbersInArray();
            // формируем массив
            sums[i] = sum;
        }

        int resultByThreads = 0;
        for (int i = 0; i < sums.length; i++) {
            resultByThreads += sums[i];
            System.out.printf("thread_%d: %d\n", i + 1, sums[i]);
        }

        System.out.println();
        System.out.println("sum threads = " + resultByThreads);
        System.out.println("sum main = " + arraySum);
    }
}
