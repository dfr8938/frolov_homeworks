package ru.dfr.homework_21;

public class SumThread extends Thread {

    private int[] array;
    private int from;
    private int to;

    public SumThread(int[] array, int from, int to) {
        this.array = array;
        this.from = from;
        this.to = to;
    }

    public void run() {
        sumNumbersInArray();
    }

    // считаем сумму элементов массива
    public int sumNumbersInArray() {
        int sum = 0;
        for (int i = this.from; i <= this.to; i++) {
            sum += this.array[i];
        }
        return sum;
    }
}
