package ru.dfr.homework_19;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        String fileName = "users.txt";

        UsersRepository userRepository = new UsersRepositoryFileImpl(fileName);
        List<User> users = userRepository.findAll();

        printTitle("findAll");
        printListUsers(users);

        User denis = new User("Denis", 32, true);
        User stepan = new User("Stepan", 33, true);
        User ivan = new User("Ivan", 33, true);
        User marina = new User("Marina", 33, true);

        notWriteRepeatUser(users, denis, userRepository);
        notWriteRepeatUser(users, stepan, userRepository);
        notWriteRepeatUser(users, ivan, userRepository);
        notWriteRepeatUser(users, marina, userRepository);

        users = userRepository.findByAge(33);

        printTitle("findByAge");
        printListUsers(users);

        users = userRepository.findByIsWorker();

        printTitle("findByIsWorker");
        printListUsers(users);


    }

    public static void printListUsers(List<User> users) {
        for (User user : users) {
            System.out.printf("Name: %s, age: %d, isWorker: %b\n", user.getName(), user.getAge(), user.getIsWorker());
        }
    }

    // проверка на повторную запись
    public static boolean equalsUsers(List<User> users, User newUser) {

        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getName().equals(newUser.getName())) {
                if (users.get(i).getAge() == newUser.getAge()) {
                    if (users.get(i).getIsWorker() == newUser.getIsWorker()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    // препятствует записи повторного юзера
    public static void notWriteRepeatUser(List<User> users, User newUser, UsersRepository userRepository) {
        if (!equalsUsers(users, newUser)) {
            userRepository.save(newUser);
        }
    }

    public static void printTitle(String str) {
        System.out.println("-------------------------");
        System.out.printf("Вывод метода %s():\n", str);
        System.out.println("-------------------------");
    }
}
