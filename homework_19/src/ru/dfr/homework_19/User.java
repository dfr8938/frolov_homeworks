package ru.dfr.homework_19;

public class User {

    private String name;
    private int age;
    private boolean isWorker;

    public User(String name, int age, boolean isWorker) {
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean getIsWorker() {
        return isWorker;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setIsWorker(boolean isWorker) {
        this.isWorker = isWorker;
    }
}
