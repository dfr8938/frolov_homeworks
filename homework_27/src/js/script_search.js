const search = document.getElementById("search");

search.addEventListener('click', () => {
    search.placeholder = "";
});

search.addEventListener('mouseover', () => {
    search.placeholder = "";
});

search.addEventListener('mouseout', () => {
    search.placeholder = "Найдётся всё";
});
